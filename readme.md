# MOOC Recherche Reproductible / Reproducibleesearch Mooc

FR

Ce dépôt contient les fichiers utiles au MOOC Recherche Reproductible.

EN

This repository contains useful files for the Reproducible Research MOOC.


---

Ceci est un test.

> **Age quod agis** :  
> fais bien ce que tu fais, qui que tu sois,  
> tu essaie de faire le mieux possible.
