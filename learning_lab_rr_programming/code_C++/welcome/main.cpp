#include <iostream>

using namespace std;

int main() {

    // char s[20];
    // cout<<"Enter your Name ";
    // // cin>>s;
    // cin.get(s,20);
    // cout<<">Welcome "<<s<<endl;
    // return 0;


    // char s[100];
    // cout<<"Enter your Name ";
    // // cin.get(s,100);
    // cin.getline(s,100);
    // cout<<"Welcome "<<s<<endl;


    // char s[100];
    // char s2[100];

    // cout<<"Enter your name ";
    // // cin.get(s,100);
    // cin.getline(s,100);

    // cout<<"Welcome "<<s<<endl;

    // cout<<"Enter your name again ";
    // // cin.get(s2,100);
    // cin.getline(s2,100);

    // cout<<"Welcome "<<s2<<endl;


    char s[100];
    char s2[100];

    cout<<"Enter your name ";
    cin.get(s,100);

    cin.ignore();		// Ignore any extra characters remaining after reading first string
    
    cout<<"Welcome "<<s<<endl;

    cout<<"Enter your name again ";
    cin.get(s2,100);

    cout<<"Welcome "<<s2<<endl;
    
    return 0;
}

